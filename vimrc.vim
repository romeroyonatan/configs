" Editor
" ----------------------------------------------------------------------------
set colorcolumn=79
" keep lines in screen when scroll
set scrolloff=3
set sidescrolloff=3
" open vertical split on right
set splitright

" open links on firefox
let g:netrw_browsex_viewer="firefox"

" Rust
let g:rustfmt_autosave = 1

" python3
let g:python3_host_prog = '/home/yromero/.pyenv/versions/neovim3/bin/python'


" Linter
" ----------------------------------------------------------------------------
" This will open the window automatically when Neomake is run
" let g:neomake_open_list = 2
let g:neomake_open_list = 0

" Max line lenght for code formatter
let g:black_linelength = 88

" javascript linter
let g:neomake_javascript_enabled_makers = ['eslint']
let g:neomake_javascript_eslint_exe = $PWD .'/node_modules/.bin/eslint'
let g:neomake_vue_eslint_exe = $PWD .'/node_modules/.bin/eslint'
let g:neomake_vue_eslint_args = ['--format', 'compact', '--plugin', 'vue', '%:p']


" Special filetype config
" ----------------------------------------------------------------------------
if has("autocmd")
  " Enable file type detection.
  filetype plugin indent on
  autocmd FileType html setlocal shiftwidth=2 tabstop=2
  autocmd FileType htmldjango setlocal shiftwidth=2 tabstop=2
  autocmd FileType vue setlocal shiftwidth=2 tabstop=2
  autocmd FileType yaml setlocal shiftwidth=2 tabstop=2
  autocmd FileType javascript.jsx setlocal shiftwidth=2 tabstop=2
endif



" Undo history
" ----------------------------------------------------------------------------
" Mantain undo history between sessions
set undofile
set undodir=~/.config/nvim/undodir



" Folding
" ----------------------------------------------------------------------------
"Enable folding
set foldenable
"Open most of the folds by default. If set to 0, all folds will be closed.
set foldlevelstart=0
"Folds can be nested. Setting a max value protects you from too many folds.
set foldnestmax=2
"Defines the type of folding
set foldmethod=indent



" Shortcuts
" ----------------------------------------------------------------------------
" fzf buffers
nnoremap ,b :Buffers<CR>
nnoremap ,h :History<CR>
"
" Move visual selection
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Control T nueva pestaña
nnoremap <c-t> <esc>:tabnew<cr>
nnoremap <c-l> :tabn<cr>
nnoremap <c-h> :tabp<cr>

" Enable and disable paste mode
set pastetoggle=<F6>

" goto next lint error
nnoremap ,, :lnext<CR>
" goto prev lint error
nnoremap ,p :lprev<CR>
" open lint errors window
nnoremap <Leader><Space>o :lopen<CR>
" close lint errors window
nnoremap <Leader><Space>c :lclose<CR>
nnoremap ,= :Black<CR>
